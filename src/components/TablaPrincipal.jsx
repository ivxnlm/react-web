import React, { useState, useEffect } from "react";
import FormularioAgregar from './FormularioAgregar'
import FormularioEditar from './FormularioEditar'

const TablaPrincipal = () => {
  // Declara una nueva variable de estado, la cual llamaremos “count”
  //Estado
  const [data, setData] = useState({}); //principal info
  const [opcion,setOpcion] = useState(0); //0 - NA //1.-Agregar //2.- Editar
  const [getData,setGetData] = useState(false); //true igual a invocar el post
  const [secret, setSecret] = useState(''); //input principal
  const [editElemento, setEditElemento] = useState({
    id:'',
    nombre:'',
    titulo:'',
    contenido:''
});//elemetno seleccionado para editar

  async function fetchData(input) {
    const req = {
      token:input
  }
    const res = await fetch("https://pokreador2.000webhostapp.com/project/getCollections.php",{
      method: 'POST',
      body:JSON.stringify(req)
        });
    res
      .json()
      .then(res => {
          setData(res.collections)
          setGetData(false)
          console.log('salida:')
          console.log(res.collections)

        })
      .catch(err => console.log(err));
  }

  
  useEffect(() => {
    if(getData)
      fetchData()
  },[getData])

  const borrarData = (item) =>{
    console.log('item: '+item.idcollection)
    deleteData(item.idcollection)
  }

  const actualizarData = (item) =>{
    console.log('Se actualiza este elemento '+item.id)
    setEditElemento({
      id:item.id,
      nombre:item.nombre,
      titulo:item.titulo,
      contenido:item.contenido
    })
    setOpcion(2)
  }

  async function deleteData(id) {
    const req = {
      token:secret,
      idcollection:id
    }
    const res = await fetch("https://pokreador2.000webhostapp.com/project/deleteCollection.php",{
      method: 'POST',
      body:JSON.stringify(req)
        });
    res
      .json()
      .then(res => {
        setGetData(true)
        console.log(res)
        })
      .catch(err => console.log(err));
  }

  const obtenerData = () => {
    console.log('obtenerData '+secret)
    fetchData(secret)
  }

  const handleText = (e) =>{
    setSecret(
        e.target.value
      );
}

  
  return (
    <div className="col-12 ">
      <div className="row contenedor-tabla">
        <div className="margin-ex div-header">
          <label>Web App</label>
        </div>
        <FormularioAgregar className="col-12" 
              opcion={opcion}
              token = {secret}
              fetchData={fetchData}
              setOpcion={setOpcion}
              setGetData={setGetData}
              editElemento = {editElemento}>
        </FormularioAgregar>
        <FormularioEditar className="col-12" 
              opcion={opcion}
              fetchData={fetchData}
              setOpcion={setOpcion}
              setGetData={setGetData}
              editElemento = {editElemento}>
        </FormularioEditar>
        <div className=" texto-busqueda col-12">
        <input type="password" id="lname" name="lname" value = {secret} onChange={handleText}/>
        <button type="button" onClick={()=>(obtenerData())} className="btn btn-default btn-sm">
                <span className="glyphicon glyphicon-trash"></span> AceptarXD
        </button>
        </div>
        
        <div className="">
          {/**Se envia la lista de elementos y las funciones para actualizar todos los checkbox o solo uno */}
          <div>
                
                <table id="tabla-iva" width="100%">
                    <thead>
                        <tr>
                            <th width="7%">Id</th>
                            <th width="17%">Nombre</th>
                            <th width="22%">Valor</th>
                            <th width="22%">Descripcion</th>
                            <th width="32%">Actions</th>
                        </tr>
                    </thead>
              <tbody>
                {
                  data!==undefined && data.length && data.map(item => {
                    const { idcollection, name, value, description } = item
                    return (<tr key={idcollection}>
                      <td>{idcollection}</td>
                      <td>{name}</td>
                      <td>{value}</td>
                      <td>{description}</td>
                      <td>
                        <button type="button" onClick={()=>actualizarData(item)} className="btn btn-default btn-sm">
                          <span className="glyphicon glyphicon-pencil"></span> Edit
                          </button>
                        <button type="button" onClick={()=>borrarData(item)} className="btn btn-default btn-sm">
                          <span className="glyphicon glyphicon-trash"></span> Delete
                          </button>
                      </td>
                    </tr>)
                  })
                }
              </tbody>
                </table>
                <div className="box todos-checkbox">
                <button type="button" onClick= {()=>{setOpcion(1)} } className="btn btn-default btn-sm">
                  <span className="glyphicon glyphicon-plus"></span> Add Element
                </button>
                </div>
            </div>
        </div>
      </div>


    </div>
  );
}

export default TablaPrincipal;
