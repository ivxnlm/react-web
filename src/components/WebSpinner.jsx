import React from "react";

const WebSpinner = (props) => {

  if(props.show === true)
    return (
      <>
      <div style={{
          position:"fixed",
          top:0,
          left:0,
          width:"100%",
          height:"100%"
        }}>
        <div 
          style={{
            left:"50%",
            top:"50%",
            zIndex:1000,
            position:"absolute"
          }}
        className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>

        </div>
      </>
    );
  else
    return null;
}

export default WebSpinner;
