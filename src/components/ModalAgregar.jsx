import React from 'react';
import { Button,Modal,Form } from 'react-bootstrap';

function ModalAgregar(props) {
  // Declara una nueva variable de estado, la cual llamaremos “count”  

    return (
      <>
      <Modal show={props.show} onHide={()=>{props.setShow(false)}} backdrop='static' centered animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>

            <Form>
              <Form.Group controlId="idDominio">
                <Form.Label>Dominio</Form.Label>
                <Form.Control type="text" placeholder="Dominio" />
              </Form.Group>
              <Form.Group controlId="idIdentificador">
                <Form.Label>Identificador</Form.Label>
                <Form.Control type="text" placeholder="Identificador" />
              </Form.Group>
              <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label>Secret</Form.Label>
                <Form.Control as="textarea" rows="3" placeholder="Secret" />
              </Form.Group>
            </Form>


        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={()=>{props.setShow(false)}}>
            Guardar Cambios
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );

}

export default ModalAgregar;