import React from 'react';

function Componente2() {
  // Declara una nueva variable de estado, la cual llamaremos “count”
  return (
    <div>
      <div className="card-footer text-muted">
        Componente 2
      </div>

    </div>
  );
}

export default Componente2;
