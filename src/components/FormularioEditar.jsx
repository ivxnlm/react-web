import React, { useState } from "react";

const FormularioEditar = (props) => {

    /**
     * Estado de componente
     */
    const [elemento, setElemento] = useState({
        id:'',
        nombre:'',
        titulo:'',
        contenido:''
    });

    const cerrarForm = () => {
        props.setOpcion(0);
    }

    const handleText = (e) =>{
            setElemento(
                { ...elemento, [e.target.name]: e.target.value }
              );
    }


    async function saveData(elemento) {
        console.log('save data: '+elemento.nombre)
        const req = {
            nombre:elemento.nombre,
            titulo:elemento.titulo,
            contenido:elemento.contenido
        }
        const res = await fetch("http://localhost:8090/v1/nota",{
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body:JSON.stringify(req)
            });
        res
          .json()
          .then(res => {
              props.setGetData(true)
              props.setOpcion(0)
              setElemento(
                {
                    id:'',
                    nombre:'',
                    titulo:'',
                    contenido:''
                }
              )
        })
          .catch(err => console.log(err));
      }


    const guardarData = (e) =>{
        e.preventDefault();
        saveData(elemento);
    }

    if(props.opcion === 2){
        
        return(
            <div>
                <div className="col-12 classCenter"><label> Editar Elemento</label></div>
                <div >
                    <div className="col-2"></div>
                    <div className="col-8 contenedor-tabla">
                        <form action="/">
                            <div>
                                <div>
                                    <label>Nombre</label>
                                </div>
                                <div>
                                    <input type="text" 
                                        name="nombre" 
                                        defaultValue={elemento.nombre}
                                        onChange={handleText}
                                        placeholder="Your name..." />
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Titulo</label>
                                </div>
                                <div>
                                    <input type="text" 
                                    name="titulo" 
                                    defaultValue = {elemento.titulo}
                                    onChange={handleText}
                                    placeholder="Your Title.." />
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Content</label>
                                </div>
                                <div>
                                    <textarea id="subject" 
                                    name="contenido" 
                                    value={elemento.contenido}
                                    onChange={handleText}
                                    placeholder="Write something.." 
                                    style={{ width: '97%' }}></textarea>
                                </div>
                            </div>
                            <div>
                                <button onClick={guardarData}>Aceptar</button>
                                <button onClick={cerrarForm}>Cancelar</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-2"></div>
                </div>
            </div>
        )
    }else{
        return(<div></div>)
    }


}

export default FormularioEditar;
