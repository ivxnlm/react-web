import React,{useState} from 'react';
import ModalAgregar from './ModalAgregar.jsx'
import ComponentTable from './ComponentTable.jsx';
import WebSpinner from './WebSpinner'

function Home() {
  // ESTADOS
  //controla el modal
  const [showModal, setShowModal] = useState(false);
  //contrlo el spinner
  const [showSpinner, setShowSpinner] = useState(false);
  //secret
  const [secret, setSecret] = useState('');
  //controla la tabla
  const [dataTable, setDataTable] = useState([]);

  //FUNCIONES
  function handleBurguer() {
    console.log('click')
    var x = document.getElementById("myTopnav");
    if (x.className === "navbar") {
      x.className += " responsive";
    } else {
      x.className = "navbar";
    }
  }

  //funcion que obtiene los datos de la peticion 
  const getData =(e) => {
    e.preventDefault();
    console.log('[GET DATA] '+secret)
    //mostrar spinner
    setShowSpinner(true);
    //invocamos el servicio
    const request = {token:secret}
    //funcion asincrona
    async function readData() {
      const res = await fetch("https://pokreador2.000webhostapp.com/project/getCollections.php",{
        method: 'POST',
        body:JSON.stringify(request)
      });
      res.json()
      .then(res => {
        console.log(res.collections)
        setDataTable(res.collections)
      })
      .catch(err => console.log(err));
      
      setShowSpinner(false);
    }
    readData()
  }



  return (
    <div>

      {/**Nav Bar */}
      <header id="myTopnav" className="navbar">
        <a href="#none" className="icon" onClick={handleBurguer}>
          <i className="fa fa-bars"></i>
        </a>
        <a className="active alwaysVisible" href="#home">Home</a>
        <a href="#about">About</a>
        <a href="#contact">Contact</a>
        <div className="search-container">
          <form action="/action_page.php">
            <input type="text" placeholder="Search.." value ={secret} onChange={(e)=>{setSecret(e.target.value)}} name="search"  />
            <button type="submit" onClick={getData}><i className="fa fa-search"></i></button>
          </form>
        </div>
      </header>

      {/**Contenedor principal */}

      <div className="container">

        {/** Headers */}
        <header className="header">
          <h1>Web</h1>
        </header>
        <header className="upperBar">
          <input type="text" name="search" placeholder="Search Item ..."/> 
          <button className="button" onClick={()=>{setShowModal(true)}}>+ Agregar</button>
        </header>

        {/**Contenido */}
        <main className="content">
          <div className="rw-responsive">
          <ComponentTable dataTable={dataTable}></ComponentTable>
          </div>
        </main>


      </div>




      {/** Footer */}
      <div className="footer">
        <h4>React Web 2020*</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, culpa?</p>
      </div>

      {/** Modal */}
      <ModalAgregar show={showModal} setShow={setShowModal} >Agregar Elemento</ModalAgregar>

      <WebSpinner show={showSpinner}></WebSpinner>


    </div>
  );
}

export default Home;
