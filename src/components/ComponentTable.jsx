import React from 'react';
import { MDBDataTable } from 'mdbreact';

function ComponentTable(props) {
  // Declara una nueva variable de estado, la cual llamaremos “count”  


  const data = {
    columns: [
      {
        label: 'Id',
        field: 'idcollection',
        sort: 'des',
        width: 150
      },
      {
        label: 'Dominio',
        field: 'name',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Identificador',
        field: 'value',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Secret',
        field: 'description',
        sort: 'asc',
        width: 200
      },
      {
        label: 'Opcion',
        field: 'opcion',
        sort: 'asc',
        width: 200
      }
    ],
    rows: props.dataTable
  };

  return (
    <MDBDataTable
      striped
      bordered
      small
      data={data}
    />
  );

}

export default ComponentTable;
