import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import TablaPrincipal from './TablaPrincipal'
import Componente2 from './Componente2'


function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

export default function App() {

  return (
    <Router>
      <div>
        <div className="topnav" id="myTopnav">
          
          <Link to="/">Home</Link>
          <Link to="/">News</Link>
          <Link to="/contact">Contact</Link>
          <Link to="/topics">Topics</Link>
          <a href="#about">About</a>
          <a href="#flex" className="icon" onClick={myFunction}>
            <i className="fa fa-bars"></i>
          </a>
        </div>
      </div>

      <Switch>
        <Route path="/contact">
        <Componente2/>
        </Route>
        <Route path="/topics">
        <TablaPrincipal />
        </Route>
        <Route path="/">
          <TablaPrincipal />
        </Route>
      </Switch>

    </Router>

  );
}

